import React, {useState} from 'react';
import './GameWin.css';
import Header from '../components/Header';
import Ingredients from '../components/Ingredients';
import Ingredient from '../components/Ingredient';
import Resources from '../components/Resources';
import ResultModalBox from '../components/ResultModalBox';
import boiler from '../images/boiler.png'

function GameWin({setScreen}) {

  const[leftIngredient, setLeftIngredient] = useState(0)
  const[rightIngredient, setRightIngredient] = useState(0)
  const[isResultModalBox, setIsResultModalBox] = useState(false)
  const[gold, setGold] = useState(1000)

  const leftIng = [
    {
      id: 1,
      name: 'Корень мандрагоры',
      price: 53
    },
    {
      id: 2,
      name: 'Жало пчелы',
      price: 78
    }
  ]
  const rightIng = [
    {
      id: 101,
      name: 'Лимонный сок',
      price: 13
    },
    {
      id: 102,
      name: 'Вода',
      price: 5
    }
  ]

  function getLeftIngredientById(){
    let result = leftIng.find(item => item.id === leftIngredient)
    return result
  }
  function getRightIngredientById(){
    let result = rightIng.find(item => item.id === rightIngredient)
    return result
  }

  return (
    <div className="GameWin">
      <Header setScreen={setScreen} />
      <div className='GameWin__ingredients'>
        <Ingredients 
          ingredientList={leftIng} 
          setLeftIngredient={setLeftIngredient} 
          setRightIngredient={setRightIngredient}
          type='left'
        />
        <Resources gold={gold} />
        <Ingredients 
          ingredientList={rightIng} 
          setLeftIngredient={setLeftIngredient} 
          setRightIngredient={setRightIngredient}
          type='right'
        />
      </div>
      <div className='GameWin__mix'>
        <div className='GameWin__boiler'>
          <img src={boiler} alt='' onClick={() => setIsResultModalBox(true)} />
          {
            leftIngredient === 0
            ?
            <div className='GameWin__item GameWin__item_left'></div>
            :
            <div className='GameWin__item GameWin__item_left'>
              <span className="Ingredient__name">{getLeftIngredientById() !== undefined ? getLeftIngredientById().name : null}</span>
              <span className="Ingredient__price">{getLeftIngredientById() !== undefined ? getLeftIngredientById().price : null}</span>
            </div>
          }
          {
            rightIngredient === 0
            ?
            <div className='GameWin__item GameWin__item_right'></div>
            :
            <div className='GameWin__item GameWin__item_right'>
              <span className="Ingredient__name">{getRightIngredientById() !== undefined ? getRightIngredientById().name : null}</span>
              <span className="Ingredient__price">{getRightIngredientById() !== undefined ? getRightIngredientById().price : null}</span>
            </div>
          }
          {
            isResultModalBox
            ?
            <ResultModalBox setIsResultModalBox={setIsResultModalBox} />
            :
            null
          }
        </div>
      </div>
      <div className='GameWin__inventory'>
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
        <Ingredient />
      </div>
    </div>
  ); 
}

export default GameWin;
