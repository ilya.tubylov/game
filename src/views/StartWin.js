import React, { useState } from 'react';
import './StartWin.css';
import Settings from '../components/Settings';

function StartWin({settings, setSettings, setScreen}) {

  const[isSettingWin, setIsSettingWin] = useState(false)

  function Login(){

    const data = {
      login: 'avenue2',
      password: '12345'
    }
    const api = 'http://localhost:9001/login'
    
    fetch(api, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(result => result.json())
    .then(result => console.log(result))

  }

  function Registration(){

    const data = {
      login: 'avenue2',
      password: '12345',
      email: 'avenue2@mail.ru'
    }
    const api = 'http://localhost:9001/registration'
    
    fetch(api, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(result => result.json())
    .then(result => console.log(result))

  }

  return (
    <div className="StartWin">
      <button onClick={Login}>Войти</button>
      <button onClick={Registration}>Регистрация</button>
      <button onClick={ () => setScreen('gameWin') }>Начать игру</button>
      <button onClick={ () => setIsSettingWin(true) }>Настройки</button>
      { isSettingWin ? <Settings setIsSettingWin={setIsSettingWin} settings={settings} setSettings={setSettings} /> : null }
    </div>
  ); 
}

export default StartWin;
