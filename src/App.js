import React, {useState} from 'react';
import './App.css';
import StartWin from './views/StartWin';
import GameWin from './views/GameWin';

function App() {

  const[settings, setSettings] = useState({
    theme: 'Светлая'
  })
  const[screen, setScreen] = useState('startWin')

  const windows = {
    startWin: <StartWin settings={settings} setSettings={setSettings} setScreen={setScreen} />,
    gameWin: <GameWin setScreen={setScreen} />
  }

  return (
    <div className="App">
      { windows[screen] }
    </div>
  );
}

export default App;
