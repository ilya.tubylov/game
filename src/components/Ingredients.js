import React from 'react';
import './Ingredients.css';
import Ingredient from './Ingredient';

function Ingredients({ingredientList, setLeftIngredient, setRightIngredient, type}) {

  function getIngredients(){
    let ingredients = []
    for(let i = 0; i < 25; i++){
      ingredients.push(
        <Ingredient 
          key={i}
          ingredient={ingredientList[i]}
          setLeftIngredient={setLeftIngredient}
          setRightIngredient={setRightIngredient}
          type={type}
        />
      )
    }
    return ingredients
  }

  return (
    <div className="Ingredients">
      { getIngredients() }
    </div>
  ); 
}

export default Ingredients;
