import React from 'react';
import './ResultModalBox.css';

function ResultModalBox({setIsResultModalBox}) {

  return (
    <div className="ResultModalBox">
        <div>
            Результат
        </div>
        <button onClick={() => setIsResultModalBox(false)}>Получить</button>
    </div>
  ); 
}

export default ResultModalBox;
