import React from 'react';
import './Header.css';
import Icons from './Icons';

function Header({setScreen}) {

  return (
    <div className="Header">
        <button onClick={() => setScreen('startWin')} className='Header__mainmenu'>
            <Icons name='mainmenu' />
        </button>
    </div>
  ); 
}

export default Header;
