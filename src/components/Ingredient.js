import React from 'react';
import './Ingredient.css';

function Ingredient({ingredient, setLeftIngredient, setRightIngredient, type}) {
  
  return (
    <>
    {
      (type === 'left' || type === 'right')
      ?
        <div className="Ingredient" onClick={() => {
            if(type === 'left'){
              setLeftIngredient(ingredient !== undefined ? ingredient.id : 0)
            }
            if(type === 'right'){
              setRightIngredient(ingredient !== undefined ? ingredient.id : 0)
            }
          }
        }>
          <span className="Ingredient__name">{ingredient !== undefined ? ingredient.name : 'пусто'}</span>
          <span className="Ingredient__price">{ingredient !== undefined ? ingredient.price : 0}</span>
        </div>
        :
        <div className="Ingredient">
          <span className="Ingredient__name"></span>
          <span className="Ingredient__price"></span>
        </div>
    }
    </>
  ); 
}

export default Ingredient;
