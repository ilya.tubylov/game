import React from 'react';
import './Resources.css';
import goldImg from '../images/gold.png'

function Resources({gold}) {

  return (
    <div className="Resources">
        <p>
            <img src={goldImg} alt='' />
            <span>{gold}</span>
        </p>
    </div>
  ); 
}

export default Resources;
